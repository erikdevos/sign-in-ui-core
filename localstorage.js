// check if localstorage value is empty and set default
if (localStorage.getItem("sidebar") === null){
	// set sidebar status to closed
  	localStorage.setItem("sidebar", "closed");
}


// set initial sidebar visibility and button text based on storage variable
if (localStorage.getItem("sidebar") === "closed"){
	$('#activitylist').hide();
	$('.hidebutton').html("show log");
	//console.log ("sidebar status is closed");
}
else if (localStorage.getItem("sidebar") === "opened"){
	$('#activitylist').show();
	$('.hidebutton').html("hide log");
	//console.log ("sidebar status is opened");
}

// on click on toggle button  check current sidebar status and act on it
$(".hidebutton").click(function(){
	// if localstorage value is closed
	if (localStorage.getItem("sidebar") === "closed" ){
		$('#activitylist').show();
		$(this).html("hide log");
		localStorage.setItem("sidebar", "opened");
		console.log ("open sesame");
	}
	// if localstorage value is opened
	else if (localStorage.getItem("sidebar") === "opened" ){
		$('#activitylist').hide();
		$(this).html("show log");
		localStorage.setItem("sidebar", "closed");
		console.log ("close it up");
	}	
});
